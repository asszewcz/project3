import { SORT } from '../../src/constants'

describe('The Home Page', function() {
  beforeEach(function () {
    // reset and seed the database prior to every test
    cy.exec('npm start')
  })
  it('successfully loads', function() {
    cy.visit('/') // change URL to match your dev URL
  })
})


describe('On Load', function() {
  it('welcome screen showing', function() {
    cy.get('.welcome-text').should('exist')
  })
  it('search bar focused', function() {
    cy.focused().should('match', '.search-bar')
  })
})


describe('Basic search bar functionality', function() {
  it('write "s" in search bar', function() {
    cy.focused().type("s")
  })
  it('clear search bar', function() {
    cy.focused().type("{backspace}")
  })
  it('Result #', function() {
    cy.get('.result-amount').contains("Results: 247")
  })
  // TODO: get
})

describe('Result on search', function() {
  it('write "Shawshank" in search bar', function() {
    cy.focused().type("Shawshank")
  })
  it('Result #', function() {
    cy.get('.result-amount').contains("Results: 1")
  })
  it('"Shawshank Redemption" found', function() {
    cy.get('.list-group-item-title').contains("Shawshank Redemption")
  })
  it('clear search bar', function() {
    cy.focused().clear()
  })
})

describe('Filter functionality 1', function() {
  it('Toggle filter button', function() {
    cy.get('.filter-button').click()
  })
  it('toggle filter checkboxes for G rating, Animation and Spanish ', function() {
    cy.get('[id="PG1"]').check()
    cy.get('[id="G2"]').check()
    cy.get('[id="L37"]').check()
  })
  it('"Toy Story 3" listed', function() {
    cy.contains('Toy Story 3').should('exist')
  })
  it('"Shawshank Redemption" not listed', function() {
    cy.contains('Shawshank Redemption').should('not.exist')
  })
  it('Reset window', function() {
    cy.get('[id="PG1"]').uncheck()
    cy.get('[id="G2"]').uncheck()
    cy.get('[id="L37"]').uncheck()
    cy.get('.search-bar').focus()
    cy.focused().clear()
  })
})


describe('Filter functionality 2', function() {
  it('toggle filter checkboxes for G rating, Animation and Spanish ', function() {
    cy.get('[id="PG1"]').check()
    cy.get('[id="G2"]').check()
    cy.get('[id="L37"]').check()
  })
  it('"Toy Story 3" listed', function() {
    cy.contains('Toy Story 3').should('exist')
  })
  it('"Shawshank Redemption" not listed', function() {
    cy.contains('Shawshank Redemption').should('not.exist')
  })
  it('Reset', function() {
    cy.get('[id="PG1"]').uncheck()
    cy.get('[id="G2"]').uncheck()
    cy.get('[id="L37"]').uncheck()
    cy.get('.search-bar').focus()
    cy.focused().clear()
  })
})




describe('Change in order', function() {
  it('changes search', function() {
    cy.focused().type('s')
    cy.contains("12 Years a Slave").should('to.exist')
  })

  it('press order button', function() {
    cy.contains("Witness for the Prosecution").should('not.exist')
    cy.get('.sort-order-button').click()
  })

  it('has changed order', function() {
    cy.get('.list-group-item-title')
    cy.contains("Witness for the Prosecution").should('exist')
  })

  it('Reset', function() {
    cy.get('.search-bar').focus()
    cy.focused().clear()
    cy.get('.sort-order-button').click()
  })
})

describe('Change in sort', function() {
  it('receives input', function() {
    cy.get('.search-bar').focus()
    cy.focused().type('a')
  })

  it('changes sort', function() {
    cy.get('.sort-button').click()
    cy.contains(SORT.YEAR).click()
    cy.get('.sort-button').contains(SORT.YEAR)
  })

  it('sorts the content', function() {
    cy.contains('The General').should('exist')
    cy.contains('12 Years a Slave').should('not.exist')
  })
})





// TODO: if starratingclick() => state update in localstorage
// Expanding Moviedetails on onClick
// Sort functionality - though prop-state
//  search that gives 2/3 results, check that order reverses? order - arrow direction
//
