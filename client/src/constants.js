export const SORT = {
  TITLE: "Title",
  YEAR: "Year",
  BOX_OFFICE: "Box office",
  IMDB_RATING: "IMDB rating",
  USER_RATING: "User rating",
  SORT: "SORT"
}

export const ORDER = {
  ASC: "ASC",
  DESC: "DESC",
  ORDER: "ORDER"
}

export const FILTER_TYPE = {
  LANGUAGE: "LANGUAGE",
  GENRE: "GENRE",
  PG: "PG"
}
