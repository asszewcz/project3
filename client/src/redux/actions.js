import { ORDER } from "../constants";

export const SELECT_SORT = "SELECT_SORT";
export const SELECT_ORDER = "SELECT_ORDER";


export const selectSort = sortType => ({
  type: SELECT_SORT,
  payload: {
    sortType: sortType
  }
});

export const selectOrder = orderType => {
  if (orderType === ORDER.ASC){
    return ({
      type: SELECT_ORDER,
      payload: {
        orderType: ORDER.DESC
      }
    })
  }
  else {
    return ({
      type: SELECT_ORDER,
      payload: {
        orderType: ORDER.ASC,
      }
    })
  }


};

export const FILTER_STATE = 'FILTER_STATE';
// TODO: move logic for state to reducer
export const filterState = (bool) => {
  let new_bool = !bool;
  return ({
  type: FILTER_STATE,
  bool: new_bool
})}

export const FILTER_RATED = 'FILTER_RATED';

export const filterRated = (rating)  => {
  return({
    type: FILTER_RATED,
    rating: rating
  })};

export const FILTER_GENRE = 'FILTER_GENRE';

export const filterGenre = (genre)  => {
  return ({
  type: FILTER_GENRE,
  genre: genre
})};

export const FILTER_LANGUAGE = 'FILTER_LANGUAGE';

export const filterLanguage = (language)  => {
  return ({
  type: FILTER_LANGUAGE,
  language: language
})};

export const CURRENT_MOVIE = 'CURRENT_MOVIE';

export const currentMovie = (title) => {
  return ({
    type: CURRENT_MOVIE,
    title: title
  });
}
