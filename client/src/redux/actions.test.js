import * as actions from './actions';
import { ORDER, SORT } from '../constants';

describe('acions', () => {
  it('Should create an action to switch order', () => {
    const expectedAction = {
      type: actions.SELECT_ORDER,
      payload: {
        orderType: ORDER.DESC
      }
    }
    expect(actions.selectOrder(ORDER.ASC)).toEqual(expectedAction);
  })

  it('Should create an action to change sort type', () => {
    const expectedAction = {
      type: actions.SELECT_SORT,
      payload: {
        sortType: SORT.TITLE
      }
    }
    expect(actions.selectSort(SORT.TITLE)).toEqual(expectedAction);
  })

  it('Should create an action to change the filter state (toggle it)', () => {
    const expectedAction = {
      type: actions.FILTER_STATE,
      bool: true
    }
    expect(actions.filterState(false)).toEqual(expectedAction);
  })

  it('Should create an action to change the filter rating', () => {
    const expectedAction = {
      type: actions.FILTER_RATED,
      rating: 'R'
    }
    expect(actions.filterRated('R')).toEqual(expectedAction);
  })

  it('Should create an action to change the filter state (toggle it)', () => {
    const expectedAction = {
      type: actions.FILTER_GENRE,
      genre: 'Crime'
    }
    expect(actions.filterGenre('Crime')).toEqual(expectedAction);
  })

  it('Should create an action to change a ', () => {
    const expectedAction = {
      type: actions.FILTER_LANGUAGE,
      language: 'Norwegian'
    }
    expect(actions.filterLanguage('Norwegian')).toEqual(expectedAction);
  })

  it('Should create an action to set a current movie', () => {
    const expectedAction = {
      type: actions.CURRENT_MOVIE,
      title: 'lorem ipsum'
    }
    expect(actions.currentMovie('lorem ipsum')).toEqual(expectedAction);
  })
})
