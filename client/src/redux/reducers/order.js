import { SELECT_ORDER } from "../actions";
import { ORDER } from "../../constants";

const initialState = ORDER.ASC;

const order = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_ORDER: {
      return action.payload.orderType
    }
    default: {
      return state;
    }
  }
}

export default order;
