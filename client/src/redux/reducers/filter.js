import { FILTER_RATED, FILTER_GENRE, FILTER_LANGUAGE, FILTER_STATE} from '../actions';


// Set initialState for the filter
const initialState = {
  filter_state : false,
  filter_rated : [],
  filter_language: [],
  filter_genre:[]
}
// TODO: find the number of different ratings, languages and countries
//       and order them. find somewhere to save the orderings.
//       implement the filter function.

function filterReducer(state = initialState, action) {
  switch(action.type){
    case FILTER_RATED:
      if (state.filter_rated.includes(action.rating)){
        let rated = state.filter_rated;
        rated.splice(rated.indexOf(action.rating), 1);
        return ({...state, filter_rated : rated});
      }else {
        let rated = state.filter_rated;
        rated.push(action.rating);
        return ({...state, filter_rated : rated});
      }
    case FILTER_LANGUAGE:
      if (state.filter_language.includes(action.language)){
        let language = state.filter_language;
        language.splice(language.indexOf(action.language), 1);
        return ({...state, filter_language : language});
      }else {
        let language = state.filter_language;
        language.push(action.language);
        return ({...state, filter_language : language});
      }
    case FILTER_GENRE:
      if (state.filter_genre.includes(action.genre)){
        let genre = state.filter_genre;
        genre.splice(genre.indexOf(action.genre), 1);
        return ({...state, filter_genre : genre});
      }else {
        let genre = state.filter_genre;
        genre.push(action.genre);
        return ({...state, filter_genre : genre});
      }
    case FILTER_STATE:
      return{
        ...state, filter_state: action.bool
      };
    default:
      return state;

};
}


export default filterReducer;
