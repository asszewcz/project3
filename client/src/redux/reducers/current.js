import { CURRENT_MOVIE } from '../actions';

const initialState = {
  current_title : ""
}

function currentReducer(state = initialState, action) {
  if (action.type === CURRENT_MOVIE) {
    return{
      current_title: action.title
    };
  } else {
      return state;
  }
}

export default currentReducer;
