import { SELECT_SORT } from "../actions";
import { SORT } from "../../constants";

const initialState = SORT.TITLE;

const sort = (state = initialState, action) => {
    switch (action.type) {
      case SELECT_SORT: {
        return action.payload.sortType;
      }
      default: {
        return state;
      }
    }
}

export default sort;
