import order from './order';
import sort from './sort';
import currentReducer from './current';
import filter from './filter';
import { ORDER, SORT } from '../../constants';
import * as actions from '../actions';

describe('order reducer', () => {
  it('should return the initial state', () => {
    expect(order(undefined, {})).toEqual(ORDER.ASC)
  })

  it('should handle an order change', () => {
    expect(order(ORDER.DESC, {
      type: actions.SELECT_ORDER,
      payload: {
        orderType: ORDER.DESC
      }
    })).toEqual(ORDER.DESC)
  })
})



describe('sort reducer', () => {
  it('should return the initial state', () => {
    expect(sort(undefined, {})).toEqual(SORT.TITLE)
  })
  it('should handle a sort change', () => {
    expect(sort(SORT.TITLE, {
      type: actions.SELECT_SORT,
      payload: { sortType: SORT.YEAR }
    })).toEqual(SORT.YEAR)
  })
})




describe('filter reducer', () => {
  it('should return the initial state', () => {
    expect(filter(undefined, {})).toEqual({
      filter_state : false,
      filter_rated : [],
      filter_language: [],
      filter_genre:[]
    })
  })
  it('should handle change in filters', () => {
    expect(filter(undefined, {
      type: actions.FILTER_RATED,
      rating: 'PG'
    })).toEqual({
      filter_state : false,
      filter_rated : ['PG'],
      filter_language: [],
      filter_genre:[]
    })
    expect(filter({
      filter_state : false,
      filter_rated : ['PG'],
      filter_language: [],
      filter_genre:[]
    }, {
      type: actions.FILTER_RATED,
      rating: 'R'
    })).toEqual({
      filter_state : false,
      filter_rated : ['PG', 'R'],
      filter_language: [],
      filter_genre:[]
    })
  })
})


describe('current movie reducer', () => {
  it('should return the initial state', () => {
    expect(currentReducer(undefined, {})).toEqual({
      current_title : ""
    })
  })
  it('should handle an input', () => {
    expect(currentReducer({ current_title: "EARLIER" }, { title: "LATER", type: actions.CURRENT_MOVIE } )).toEqual({
      current_title: "LATER"
    })
  })
})
