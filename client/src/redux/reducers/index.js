import {combineReducers } from 'redux';
import filterReducer from './filter'
import currentReducer from './current'
import sort from './sort';
import order from './order';

export default combineReducers({
  sort,
  order,
  filterReducer,
  currentReducer
})
