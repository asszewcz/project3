import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, Accordion } from 'react-bootstrap';
import axios from 'axios';

import { filterState, currentMovie } from './redux/actions';

import MovieDetail from './components/MovieDetail';
import Filter from './components/Filter';
import SortButton from './components/SortButton'
import PagePagination from './components/PagePagination.js';

import './App.css';

const logo = require('./images/logo.svg');

/*
 * Main component. Only component that is a React class and not a functional
 * component. Started with classes, and as functionality grew we did not
 * have time to convert this into a functional one.
 *
 * Most functionality is based on calls to the back end as we wanted
 * to have most of the data processing on the server. This to reduce load
 * on the client.
 *
 * We initially wanted to have error handling with messages to the user
 * when something went wrong, but we did not have time to develop this.
 */
class App extends Component {
  constructor(props) {
    super(props);

    axios.defaults.baseURL = 'http://it2810-29.idi.ntnu.no:3001';

    this.state = {
      movies: [],
      error: "",
      noOfPages: 0,
      activePage: 1
    }

    this.pageChanged = false;

    this.searchString = "";
    this.from = 0;
    this.to = 10;

    this.getMovies = this.getMovies.bind(this);
    this.searchChange = this.searchChange.bind(this);
    this.changePage = this.changePage.bind(this);
  }

  // Changes the current search string and updates the result based on
  // it.
  searchChange(searchInput) {
    this.searchString = searchInput;
    this.getMovies();
  }

  // The meat of the application. We send the relevant data to
  // the API to get an array of all movie titles based on the input
  // from the user. We did not want to load all the movie details here
  // as this reduced performance.
  getMovies = () => {
    // If the pagination is not the caller of this function, reset
    // pagination to default.
    if(!this.pageChanged) {
      this.from = 0;
      this.to = 10;
      this.setState({ activePage: 1 });
    }

    // As the redux state is an object, it has to be converted to an
    // array such that the API can handle the data.
    let filterLanguagesArr = Array.from(this.props.filter_languages);
    let filterRatingsArr = Array.from(this.props.filter_ratings);
    let filterGenresArr = Array.from(this.props.filter_genres);

    axios.post('/api/GetMovieTitles', null, {
      params: {
        searchString: this.searchString,
        from: this.from,
        to: this.to,
        sortType: this.props.sort_type,
        sortOrder: this.props.sort_order,
        filterLanguages: filterLanguagesArr,
        filterRatings: filterRatingsArr,
        filterGenres: filterGenresArr
      }
    }).then((res) => {
      if (res.data.success) {
        this.setState({
          movies: res.data.data,
          noOfPages: res.data.pages,
          resultCount: res.data.hits
        });
      } else {
        this.setState({ error: res.data.error });
      }

      this.pageChanged = false;
    });
  }

  // Pagination logic. When the user changes page, load the relevant
  // movie list.
  changePage = (newPage) => {
    this.setState({ activePage: newPage });

    this.from = newPage * 10 - 10;
    this.to = newPage * 10;
    this.pageChanged = true;

    this.getMovies();
  }

  render() {
    return(
      <div className="main-container">
          <div className="logo-wrapper">
            <img src={logo} alt="logo" />
          </div>
          <div className="title-wrapper">
            <p> Online Movie Gathering </p>
          </div>
          <div className="search-wrapper">
            <Form.Control
              className="search-bar"
              autoFocus size="lg"
              type="text"
              placeholder="Search..."
              onChange={change => this.searchChange(change.target.value)}
            />
          </div>
          <div className="filter-wrapper">
            <Button
              className= "filter-button"
              variant="outline-secondary"
              size="lg"
              onClick={() => this.props.filterState(this.props.filter_state)} block
            >Filter</Button>
          </div>
          <div className="sort-wrapper">
              <SortButton getMovies = {this.getMovies} />
          </div>
          <div
            className={this.props.filter_state ? "filter-options-active" : "filter-options"}
          >
            <Filter getMovies = {this.getMovies}/>
          </div>
        <div className="main-content">
        {
          this.state.movies.length === 0 ?
            <div className="welcome-text">
              No results found. Search by typing a movie title in the searchbar.
            </div>
          :
          <>
            <div className="result-amount">
              Results: {this.state.resultCount}
            </div>
            <Accordion>
            {this.state.movies.map((movie, index) => {
              return(
                <MovieDetail key={"movie-detail-" + movie.Title} title={movie.Title} />
              )
            })}
            </Accordion>
          </>
        }
        </div>
        <div className="pagination-wrapper">
          <PagePagination
            pages={this.state.noOfPages}
            activePage={this.state.activePage}
            changePage={this.changePage}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    filter_state : state.filterReducer.filter_state,
    filter_languages : state.filterReducer.filter_language,
    filter_ratings : state.filterReducer.filter_rated,
    filter_genres : state.filterReducer.filter_genre,
    sort_type : state.sort,
    sort_order : state.order
    }
};

export default connect(mapStateToProps, {filterState, currentMovie})(App);
