import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';

import { filterRated, filterGenre, filterLanguage } from '../redux/actions';
import { FILTER_TYPE } from '../constants'

import './Filter.css'

/*
 * Component to render and handle functionality for the filters
 */
const Filter =  ({PGActive, filterRated, filterLanguage, filterGenre, getMovies}) => {
  // TODO: If we have the time, consider imporoving the checkboxes with an select/
  // deselect all button.
  // const [PGClear, setPGClear] = useState(false);
  // <Button onClick={ () => setPGClear(!PGClear)}>Unselect/select all</Button>

  // TODO: make constants instead of fuckings hard coded searchInput
  // <Form.Check type={'checkbox'}  onClick={ () =>filterRated(FILTER.PG[0])} id={"PG1"} label={"G"}/>

  // Function to update the search result based on
  // selected filters
  const combinedFunction = filter => {
    switch (filter[0]) {
      case FILTER_TYPE.PG:
        filterRated(filter[1]);
        getMovies();
        break;
      case FILTER_TYPE.LANGUAGE:
        filterLanguage(filter[1]);
        getMovies();
        break;
      case FILTER_TYPE.GENRE:
        filterGenre(filter[1]);
        getMovies();
        break;
      default:
        break;
    }
  }

  return (
    <Form as={Row}>
      <Form.Group as={Col}>
        <Form.Label column sm="15">
          <b>PG-Rating</b>
        </Form.Label>
        <Col className="col-class" sm="15">
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG,"G"])} id={"PG1"} label={"G"}/>
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG, "PG"])} id={"PG2"} label={"PG"}/>
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG, "PG-13"])} id={"PG3"} label={"PG-13"}/>
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG,"R"])} id={"PG4"} label={"R"}/>
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG, "Not Rated"])} id={"PG6"} label={"Not Rated"}/>
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG, "Passed"])} id={"PG7"} label={"Passed"}/>
          <Form.Check type={'checkbox'}  onClick={ () =>combinedFunction([FILTER_TYPE.PG, "Approved"])} id={"PG8"} label={"Approved"}/>
        </Col>
      </Form.Group>
      <Form.Group  as={Col}>
        <Form.Label column sm="15">
          <b>Genre</b>
        </Form.Label>
        <Col className="col-class" sm="15">
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Action"])} id={"G1"} label={"Action"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Animation"])} id={"G2"} label={"Animation"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Biography"])} id={"G3"} label={"Biography"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Comedy"])} id={"G4"} label={"Comedy"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Crime"])} id={"G5"} label={"Crime"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Drama"])} id={"G6"} label={"Drama"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Family"])} id={"G8"} label={"Family"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Fantasy"])} id={"G9"} label={"Fantasy"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Film-Noir"])} id={"G10"} label={"Film-Noir"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Horror"])} id={"G11"} label={"Horror"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "History"])} id={"G12"} label={"History"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Musical"])} id={"G13"} label={"Musical"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Mystery"])} id={"G14"} label={"Mystery"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Romance"])} id={"G15"} label={"Romance"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Sci-Fi"])} id={"G16"} label={"Sci-Fi"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Short"])} id={"G17"} label={"Short"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Sport"])} id={"G18"} label={"Sport"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Thriller"])} id={"G19"} label={"Thriller"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "War"])} id={"G20"} label={"War"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.GENRE, "Western"])} id={"G21"} label={"Western"}/>
        </Col>
      </Form.Group>
      <Form.Group  as={Col}>
        <Form.Label column sm="15">
          <b>Language</b>
        </Form.Label>
        <Col className="col-class" sm="15">
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Arabic"])} id={"L1"} label={"Arabic"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "American Sign Language"])} id={"L2"} label={"American Sign Language"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Belarusian"])} id={"L3"} label={"Belarusian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Cantonese"])} id={"L4"} label={"Cantonese"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Chinese"])} id={"L5"} label={"Chinese"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Czech"])} id={"L6"} label={"Czech"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Danish"])} id={"L7"} label={"Danish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "English"])} id={"L8"} label={"English"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Esperanto"])} id={"L9"} label={"Esperanto"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "French"])} id={"L10"} label={"French"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "German"])} id={"L11"} label={"German"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Greek"])} id={"L12"} label={"Greek"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Hebrew"])} id={"L13"} label={"Hebrew"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Hindi"])} id={"L14"} label={"Hindi"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Hmong"])} id={"L15"} label={"Hmong"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Hungarian"])} id={"L16"} label={"Hungarian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Italian"])} id={"L17"} label={"Italian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Japanese"])} id={"L18"} label={"Japanese"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Korean"])} id={"L19"} label={"Korean"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Kurdish"])} id={"L20"} label={"Kurdish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Latin"])} id={"L21"} label={"Latin"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Mandarin"])} id={"L22"} label={"Mandarin"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Nepali"])} id={"L23"} label={"Nepali"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Norwegian"])} id={"L24"} label={"Norwegian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "North American Indian"])} id={"L25"} label={"North American Indian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Old English"])} id={"L26"} label={"Old English"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Persian"])} id={"L27"} label={"Persian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Polish"])} id={"L28"} label={"Polish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Portuguese"])} id={"L29"} label={"Portuguese"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Punjabi"])} id={"L30"} label={"Punjabi"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Quenya"])} id={"L31"} label={"Quenya"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Russian"])} id={"L32"} label={"Russian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Scottish Gaelic"])} id={"L33"} label={"Scottish Gaelic"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Shanghainese"])} id={"L34"} label={"Shanghainese"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Sicilian"])} id={"L35"} label={"Sicilian"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Sindarin"])} id={"L36"} label={"Sindarin"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Spanish"])} id={"L37"} label={"Spanish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Swedish"])} id={"L38"} label={"Swedish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Swahili"])} id={"L39"} label={"Swahili"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Tamil"])} id={"L40"} label={"Tamil"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Telugu"])} id={"L41"} label={"Telugu"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Thai"])} id={"L42"} label={"Thai"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Turkish"])} id={"L43"} label={"Turkish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Urdu"])} id={"L44"} label={"Urdu"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Vietnamese"])} id={"L45"} label={"Vietnamese"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Xhosa"])} id={"L46"} label={"Xhosa"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Yiddish"])} id={"L47"} label={"Yiddish"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "Zulu"])} id={"L48"} label={"Zulu"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "None"])} id={"L49"} label={"None (sunset boulevard)"}/>
          <Form.Check type={'checkbox'} onClick={ () =>combinedFunction([FILTER_TYPE.LANGUAGE, "N/A"])} id={"L50"} label={"N/A (your name)"}/>
        </Col>
      </Form.Group>
    </Form>
  );
};

export default connect(null, { filterRated, filterLanguage, filterGenre} )(Filter)
