import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card } from 'react-bootstrap';

import { currentMovie } from '../redux/actions';
import { connect } from 'react-redux';

import './MovieDetail.css';
import InterestOverTime from './InterestOverTime';

/**
 * Component shown when selecting a movie in the search result list.
 * Renders all relevant information about the selected movie.
 * This component also handles user rating input. The user rating
 * is stored in localStorage of the user.
 */
const MovieDetailBody = ({ title }) => {
  const [movieDetail, setMovieDetail] = useState(0);
  const [rating, setRating] = useState(0);
  const userGrade = [1, 2, 3, 4, 5];

  // Stores the given user rating in the DB and updates the data in
  // frontend
  function handleRating(rating) {
    axios.post('/api/UpdateUserRating', null, {
      params: {
        Title: title,
        UserRating: rating
      }
    }).then(res => {
      if (res.data.success) {
        localStorage.setItem(movieDetail.Title + "-Rating", rating);
        setRating(rating);

        axios.get('/api/GetMovieDetail', {
          params: {
            searchString: movieDetail.Title
          }
        }).then(res => {
          if (res.data.success)
            movieDetail.UserRating = res.data.data.UserRating;
        });
      }
    });
  }

  // Hook to get the rating the current user has given the selected
  // movie.
  useEffect(() => {
    let userRate = localStorage.getItem(title + "-Rating");

    if (userRate !== null) {
      setRating(userRate);
    }
  }, [rating, title]);

  // When the current movie title changes in store, get the relevant
  // movie detail from backend
  useEffect(() => {
    axios.get('/api/GetMovieDetail', {
      params: {
        searchString: title
      }
    }).then(res => {
      setMovieDetail(res.data.data);
    });
  }, [title]);

  return(
    <Card.Body className="movie-details-body">
      <div className="movie-details-body-info">
        <div className="movie-details-body-poster">
          <img src={movieDetail.Poster} alt="Poster not available" />
        </div>
        <div className="movie-details-body-text">
          <div className="movie-details-body-text-header"><h1>Title:</h1> {title}</div>
          <div className="movie-details-body-text-header"><h1>Year:</h1> {movieDetail.Year}</div>
          <div className="movie-details-body-text-header"><h1>Actors:</h1> {movieDetail.Actors}</div>
          <div className="movie-details-body-text-header"><h1>Director:</h1> {movieDetail.Director}</div>
          <div className="movie-details-body-text-header"><h1>Country:</h1> {movieDetail.Country}</div>
          <div className="movie-details-body-text-header"><h1>imdbRating:</h1> {movieDetail.imdbRating}</div>
          <div className="movie-details-body-text-header"><h1>Your rating:</h1>
            {userGrade.map(index => {
              if (rating !== null) {
                if (rating >= index) {
                  return(
                    <i
                      key={movieDetail.Title + "-normal-active-" + index}
                      className="inline-icon material-icons active"
                      onClick={() => handleRating(index)}
                    >grade</i>
                  )
                } else {
                  return(
                    <i
                      key={movieDetail.Title + "-outline-active-" + index}
                      className="inline-icon material-icons-outlined active"
                      onClick={() => handleRating(index)}
                    >grade</i>
                  )
                }
              } else {
                return(
                  <i
                    key={movieDetail.Title + "-outline-not-rated-active-" + index}
                    className="inline-icon material-icons-outlined active"
                    onClick={() => handleRating(index)}
                  >grade</i>)
              }
            })}
          </div>
        </div>
        <div className="movie-details-body-chart">
          <InterestOverTime title={movieDetail.Title} />
        </div>
        <div className="movie-details-body-text-plot">
          {movieDetail.Plot}
        </div>
      </div>
    </Card.Body>
  )
}

const mapStateToProps = state => {
  return { title: state.currentReducer.current_title }
}

export default connect(mapStateToProps, { currentMovie })(MovieDetailBody);
