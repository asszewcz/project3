import React from 'react';
import Filter from './Filter';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import store from '../redux/store';


it('Filter renders correctly', () => {
  const tree = renderer.create(<Provider store={store} ><Filter /></Provider>).toJSON();
  expect(tree).toMatchSnapshot();
})
