import React from 'react';
import MovieDetail from './MovieDetail';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import store from '../redux/store';


it('Movie detail renders correctly', () => {
  const tree = renderer.create(<Provider store={store} ><MovieDetail /></Provider>).toJSON();
  expect(tree).toMatchSnapshot();
})
