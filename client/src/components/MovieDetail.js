import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Accordion, Card } from 'react-bootstrap';

import { currentMovie } from '../redux/actions';
import { connect } from 'react-redux';

import MovieDetailBody from './MovieDetailBody';

import './MovieDetail.css';

/**
 * Renders one search result with movie title and average user rating in header.
 * Uses Bootstraps Accordion to get nice animations.
 */
const MovieDetail = ({ currentMovie, title, title_state }) => {
  const [movieDetail, setMovieDetail] = useState(0);

  const userGrade = [1, 2, 3, 4, 5];

  useEffect(() => {
    axios.get('/api/GetMovieHeaders', {
      params: {
        Title: title
      }
    }).then(res => {
      if (res.data.success) {
        setMovieDetail(res.data.data);
      }
    });
  }, [title]);

  const selectMovie = (title) => {
    currentMovie(title);
  }

  return(
    <Card className="list-group">
      <Accordion.Toggle
        as={Card.Header}
        eventKey={movieDetail.Title}
        onClick={e => selectMovie(movieDetail.Title)}
        className="list-group-item"
      >
        <div className="list-group-item-title">
          {movieDetail.Title}
        </div>
        <div className="list-group-item-grade">
          {userGrade.map(index => {
            if (movieDetail.UserRating >= index) {
              return (<i key={index} className="material-icons">grade</i>)
            } else {
              return (<i key={index} className="material-icons-outlined">grade</i>)
            }
          })}
        </div>
      </Accordion.Toggle>
      <Accordion.Collapse eventKey={movieDetail.Title}>
        {movieDetail.Title === title_state ? <MovieDetailBody /> : <></>}
      </Accordion.Collapse>
    </Card>
  )
}

 const mapStateToProps = state => {
   return { title_state: state.currentReducer.current_title }
 }

export default connect(mapStateToProps, { currentMovie })(MovieDetail);
