import React, { useState, useEffect } from 'react';
import axios from 'axios';

import { currentMovie } from '../redux/actions';
import { connect } from 'react-redux';

import { Chart } from 'react-google-charts';

/**
 * Component to get and show the Google search trend over the last week.
 * Param: title - movie title to search for
 * return: the component HTML as an Area chart.
 *
 * An error check is added so the chart gets the related data
 * before it renders. This makes the site behave more natural.
 */
const InterestOverTime = ({ title }) => {
  const [chartData, setChartData] = useState(['Date', 'Intereset'], ['Temp', 10]);
  const [error, setError] = useState(true);

  // Called each time a new title is passed to the component
  useEffect(() => {
    axios.get('/api/MovieTrend', { params: { Title: title }})
    .then(res => {
      if (res.data.success === true) {
        setChartData(res.data.data);
        setError(false);
      } else {
        setError(true);
      }
    });
  }, [title]);

  return(
    <>
      {!error ?
        <Chart
          width={300}
          height={300}
          chartType="AreaChart"
          loader={<div>Loading chart</div>}
          data={chartData}
          options={{
            title: 'Interest over time on Google'
          }}
        />
      : <b>No data found on Google Trends</b> }
    </>
  )
}

const mapStateToProps = state => {
  return { title: state.currentReducer.current_title }
}

export default connect(mapStateToProps, { currentMovie })(InterestOverTime);
