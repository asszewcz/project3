import React from 'react';
import SortButton from './SortButton';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import store from '../redux/store';


it('Sort button renders correctly', () => {
  const tree = renderer.create(<Provider store={store} ><SortButton /></Provider>).toJSON();
  expect(tree).toMatchSnapshot();
})
