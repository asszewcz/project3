import React from 'react';
import { connect } from 'react-redux';

import { Dropdown, ButtonToolbar, Button } from 'react-bootstrap';
import './SortButton.css';

import { selectSort, selectOrder } from "../redux/actions";
import { SORT, ORDER } from "../constants";

/*
 * Component to render and handle the sort functionality.
 */
const SortButton = ({ activeSort, selectSort, activeOrder, selectOrder, getMovies }) => {

  // Updates the search result as sort is selected. setTimeout is implemented
  // to simulate async functionality. Did not have time to develop a better
  // solution
  const combinedFunction = input => {
    switch (input[0]) {
      case SORT.SORT:
        selectSort(input[1]);
        setTimeout(function() {
          getMovies();
        }, 250);
        break;
      case ORDER.ORDER:
        selectOrder(input[1]);
        setTimeout(function() {
          getMovies();
        }, 250);
        getMovies();
        break;
      default:
        break;
    }
  }

  return(
    <div>
      <Dropdown >
        <Dropdown.Toggle
          className="sort-button"
          variant="outline-secondary"
          id="dropdown-basic"
        >
          {activeSort}
        </Dropdown.Toggle>
        <Dropdown.Menu className="sort-button-drop">
          <Dropdown.Item
            onClick={() => {combinedFunction([SORT.SORT, SORT.TITLE])}}
          >{SORT.TITLE}</Dropdown.Item>
          <Dropdown.Item
            onClick={() => {combinedFunction([SORT.SORT, SORT.YEAR])}}
          >{SORT.YEAR}</Dropdown.Item>
          <Dropdown.Item
            onClick={() => {combinedFunction([SORT.SORT, SORT.BOX_OFFICE])}}
          >{SORT.BOX_OFFICE}</Dropdown.Item>
          <Dropdown.Item
            onClick={() => {combinedFunction([SORT.SORT, SORT.IMDB_RATING])}}
          >{SORT.IMDB_RATING}</Dropdown.Item>
          <Dropdown.Item
            onClick={() => {combinedFunction([SORT.SORT, SORT.USER_RATING])}}
          >{SORT.USER_RATING}</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <ButtonToolbar>
        <Button
          className="sort-order-button"
          variant="outline-secondary"
          onClick={() => {combinedFunction([ORDER.ORDER, activeOrder])}}
        >
          {activeOrder === "ASC" ?  '↑' : '↓'}
        </Button>
      </ButtonToolbar>
    </div>
  )
}

const mapStateToProps = state => {
  return { activeSort: state.sort, activeOrder: state.order };
};

export default connect(mapStateToProps, { selectSort, selectOrder })(SortButton);
