import React from 'react';
import { Pagination } from 'react-bootstrap';

/*
 * Component to render the pagination. All functionality resides
 * in App.js.
 */
const PagePagination = ({ pages, activePage, changePage }) => {
  return(
    <Pagination>
      <Pagination.First
        onClick={() => changePage(1)}
        disabled={activePage === 1}
      />
      <Pagination.Prev
        onClick={() => changePage(activePage - 1)}
        disabled={activePage === 1}
      />
      {[...Array(pages).keys()].map(page => {
        if(page + 1 === 1) {
          return(
            <Pagination.Item
              active={page + 1 === activePage}
              onClick={() => changePage(page + 1)}
              key={page}
            >
              {page + 1}
            </Pagination.Item>
          )
        } else if(page + 1 === pages) {
          return(
            <Pagination.Item
              active={page + 1 === activePage}
              onClick={() => changePage(page + 1)}
              key={page}
            >
              {page + 1}
            </Pagination.Item>
          )
        } else if(page + 1 < activePage + 3 && page + 1 > activePage - 3) {
          return(
            <Pagination.Item
              active={page + 1 === activePage}
              onClick={() => changePage(page + 1)}
              key={page}
            >
              {page + 1}
            </Pagination.Item>
          )
        } else if(page + 1 === activePage + 3) {
          return(
            <Pagination.Ellipsis key={page} />
          )
        } else if(page + 1 === activePage - 3) {
          return(
            <Pagination.Ellipsis key={page} />
          )
        } else {
          return(<></>)
        }
      })}
      <Pagination.Next
        onClick={() => changePage(activePage + 1)}
        disabled={activePage === pages}
      />
      <Pagination.Last
        onClick={() => changePage(pages)}
        disabled={activePage === pages}
      />
    </Pagination>
  )
}

export default PagePagination;
