from requests import session
import os
import json

move_titles = ["The Shawshank Redemption", "The Godfather", "The Godfather: Part II",
"The Dark Knight", "Angry Men", "Schindler's List", "The Lord of the Rings: Return of the King",
"Pulp Fiction", "The Good, the Bad and the Ugly", "Fight Club", "The Lord of the Rings: The Fellowship of the Ring",
"Star Wars: Episode V - The Empire Strikes Back", "Forrest Gump", "Inception",
"The Lord of the Rings: The Two Towers", "One Flew Over the Cuckoo's Nest",
"Goodfellas", "The Matrix", "Seven Samurai", "Star Wars: Episode IV - A New Hope",
"City of God", "Se7en", "The Silence of the Lambs", "It's a Wonderful Life",
"Life Is Beautiful", "The Usual Suspects", "Léon: The Professional", "Spirited Away",
"Saving Private Ryan", "Once Upon a Time in the West", "American History X",
"Interstellar", "Casablanca", "Psycho", "City Lights", "The Green Mile",
"The Intouchables", "Modern Times", "Raiders of the Lost Ark", "Rear Window",
"The Pianist", "The Departed", "Terminator 2: Judgment Day", "Back to the Future",
"Whiplash", "Gladiator", "Memento", "The Prestige", "The Lion King", "Apocalypse Now",
"Alien", "Sunset Boulevard", "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb",
"The Great Dictator", "Cinema Paradiso", "The Lives of Others", "Grave of the Fireflies",
"Paths of Glory", "Django Unchained", "The Shining", "WALL·E", "American Beauty",
"The Dark Knight Rises", "Princess Mononoke", "Aliens", "Oldboy", "Once Upon a Time in America",
"Witness for the Prosecution", "Das Boot", "Citizen Kane", "North by Northwest",
"Vertigo", "Star Wars: Episode VI - Return of the Jedi", "Reservoir Dogs",
"Braveheart", "M", "Dangal", "Requiem for a Dream", "Amélie", "A Clockwork Orange",
"Like Stars on Earth", "Lawrence of Arabia", "Taxi Driver", "Double Indemnity",
"Eternal Sunshine of the Spotless Mind", "Amadeus", "To Kill a Mockingbird",
"Toy Story 3", "Full Metal Jacket", "2001: A Space Odyssey", "The Sting",
"Singin' in the Rain", "Toy Story", "Bicycle Thieves", "The Kid", "Inglourious Basterds",
"Snatch", "3 Idiots", "Your Name", "Monty Python and the Holy Grail", "L.A. Confidential",
"My Father and My Son", "For a Few Dollars More", "The Hunt", "Scarface",
"Good Will Hunting", "The Apartment", "Rashomon", "A Separation", "Metropolis",
"Indiana Jones and the Last Crusade", "All About Eve", "Yojimbo", "Batman Begins",
"Up", "Some Like It Hot", "The Treasure of the Sierra Madre", "Unforgiven",
"Downfall", "Raging Bull", "Die Hard", "The Third Man", "Children of Heaven",
"Heat", "Logan", "The Great Escape", "La La Land", "Chinatown", "Ikiru",
"Pan's Labyrinth", "My Neighbor Totoro", "Inside Out", "Ran", "The Gold Rush",
"On the Waterfront", "The Secret in Their Eyes", "Room", "The Bridge on the River Kwai",
"Blade Runner", "Howl's Moving Castle", "Judgment at Nuremberg", "Incendies",
"The Seventh Seal", "Lock, Stock and Two Smoking Barrels", "Mr. Smith Goes to Washington",
"Casino", "A Beautiful Mind", "Andrei Rublev", "The Elephant Man", "Wild Strawberries",
"V for Vendetta", "The General", "The Wolf of Wall Street", "Warrior", "The Bandit",
"Trainspotting", "Sunrise", "Gran Torino", "Dial M for Murder", "Hacksaw Ridge",
"The Deer Hunter", "The Big Lebowski", "Fargo", "Gone with the Wind", "The Sixth Sense",
"The Thing", "Tokyo Story", "Rang De Basanti", "Finding Nemo", "No Country for Old Men",
"The Passion of Joan of Arc", "Cool Hand Luke", "A Wednesday", "Rebecca",
"How to Train Your Dragon", "There Will Be Blood", "Kill Bill: Vol. 1", "Come and See",
"Mary and Max", "Into the Wild", "Gone Girl", "Life of Brian", "It Happened One Night",
"Shutter Island", "Platoon", "Hotel Rwanda", "Rush", "Wild Tales", "Network",
"The Wages of Fear", "Wonder Woman", "In the Name of the Father", "Stand by Me",
"The  Blows", "Spotlight", "The Grand Budapest Hotel", "Mad Max: Fury Road",
"12 Years a Slave", "Ben-Hur", "Munna Bhai M.B.B.S.", "Persona", "Butch Cassidy and the Sundance Kid",
"Million Dollar Baby", "Amores Perros", "Jurassic Park", "Memories of Murder",
"The Maltese Falcon", "The Nights of Cabiria", "Stalker", "The Princess Bride",
"The Truman Show", "Hachi: A Dog's Tale", "Nausicaä of the Valley of the Wind",
"Sholay", "Star Wars: The Force Awakens", "Before Sunrise", "The Grapes of Wrath",
"Hera Pheri", "Harry Potter and the Deathly Hallows: Part 2", "Rocky",
"Touch of Evil", "Prisoners", "Gandhi", "Annie Hall", "Diabolique", "Donnie Darko",
"Catch Me If You Can", "Monsters, Inc.", "The Bourne Ultimatum", "The Terminator",
"The Wizard of Oz", "Groundhog Day", "Barry Lyndon", "La Haine", "Twelve Monkeys",
"Jaws", "Infernal Affairs", "The Best Years of Our Lives", "Guardians of the Galaxy Vol. 2",
"Gangs of Wasseypur", "The Battle of Algiers", "Dog Day Afternoon", "The Help",
"Beauty and the Beast", "Andaz Apna Apna", "What Ever Happened to Baby Jane?",
"In the Mood for Love", "Pirates of the Caribbean: The Curse of the Black Pearl",
"PK"]

not_found = []
found = []

with session() as c:
    for title in move_titles:
        response = c.get('http://www.omdbapi.com/?apikey=779dc357&t="' + title + '"')
        responseJSON = json.loads(response.text)

        if (responseJSON["Response"] == "False"):
            not_found.append(title)
        else:
            found.append(responseJSON)

with open("movie_details.json", "w") as write_file:
    for k in found:
        write_file.write(json.dumps(k) + ", ")

with open("not_found.txt", "w") as write_file:
    for el in not_found:
        write_file.write(el + ", ")
