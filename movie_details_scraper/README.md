## Movie Details Scraper
This small `Python` script automatically creates a `.json` file containing information on [IMDBs Top 250](https://www.imdb.com/chart/top?ref_=nv_mv_250) movie list using the [Open Movie Database API](https://omdbapi.com/). The list of movie titles were gathered by copying all the titles from [IMDB](https://www.imdb.com) and some clever use of `regex` in Atom.

The `.json` file was then inserted into our `MongoDB` collection using
```bash
mongoimport --db project3 --collection movies --file movie_details.json
```
directly on our VM.
