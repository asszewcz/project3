const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

var movieRouter = require('./routes/movies');

// Initial DB connection
const API_PORT = 3001;
const app = express();
const router = express.Router();

const dbRoute = 'mongodb://gruppe29:sniperxXx42069@it2810-29.idi.ntnu.no:27017/project3';

// Connect to DB
mongoose.connect(dbRoute, { useUnifiedTopology: true, useNewUrlParser: true });

let db = mongoose.connection;

// Logging to console just to clarify if the connection was successful
db.once('open', () => console.log('Connected to database'));

db.on('error', console.error.bind(console, 'MongoDB connection error: '));

app.use(cors());
app.use('/api', movieRouter);

app.listen(API_PORT, () => console.log('Listening on port ', API_PORT));
