var express = require('express');
var router = express.Router();

const googleTrends = require('google-trends-api');

const Movies = require('../Schemas/Movies');

 /* GET movie titles
  *
  * Params: {
  *   searchString: searches for titles containing this string,
  *   from: start index of search,
  *   to: end index of search,
  *   sortType: the sort type. Get the current type from state,
  *   sortOrder: the sort order. Get the current order from state,
  *   filterLanguages: Array with all language filters,
  *   filterRatings: Array with all rated filters,
  *   filterGenres: Array with all genre filters
  * }
  * Returns: {
  *   success: Boolean,
  *   data: Array of results,
  *   pages: no of pages with hits
  * }
  *
  * Gets all movie titles containing the searchString in title.
  * Only returns the number of hits wanted
  */
router.post('/GetMovieTitles', function(req, res, next) {
  if(req.query.filterLanguages === undefined)
    req.query.filterLanguages = [];
  if(req.query.filterGenres === undefined)
    req.query.filterGenres = [];
  if(req.query.filterRatings === undefined)
    req.query.filterRatings = [];

  let numberOfHits = Number(req.query.to) - Number(req.query.from);

  let sortBy = sorting(req.query.sortOrder, req.query.sortType);

  let filters = filter(
    req.query.filterLanguages,
    req.query.filterGenres,
    req.query.filterRatings
  );

  var query = {
    $and: [
      {$or: filters[0]},
      {$or: filters[1]},
      {$or: filters[2]}
    ],
    "Title": { $regex: req.query.searchString, $options: "i" }
  };

  // We want the count query to finish before getting the dataset from
  // MongoDB. Using a promise to get async functionality.
  var hitsPromise = () => {
    return new Promise(function(resolve, reject) {
      var hitsQuery = Movies.countDocuments(query).sort(sortBy);
      hitsQuery.exec(function(err, data) {
        err ? reject(err) : resolve(data);
      });
    });
  }

  var hitsResult = async () => {
    var result = await(hitsPromise());

    // The find query that actually returns the dataset from database
    var findQuery = Movies.find(query).sort(sortBy).skip(Number(req.query.from)).limit(Number(numberOfHits));
    findQuery.exec(function(err, data) {
      if (err) return res.json({ success: false, error: err });

      // Calculating the number of pages needed for pagination
      let noOfPages = Math.ceil(result / Number(numberOfHits));

      if(isNaN(noOfPages))
        noOfPages = 1;

      return res.json({ success: true, data: data, pages: noOfPages, hits: result });
    });
  }

  hitsResult();
});

/* GET movie details
 *
 * Params: {
 *   searchString: title to find details of
 * }
 * Returns: returns the details of the given movie
 *
 * Used to get the details of one givem movie. This to reduse inital load on
 * search.
 */
router.get('/GetMovieDetail', function(req, res, next) {
  var query = Movies.findOne({ "Title": {'$regex': req.query.searchString, '$options': 'i'} });

  query.exec(function(err, data) {
    if (err) return res.json({ success: false, error: err });

    return res.json({ success: true, data: data });
  });
});

// Filer logic. Converts the filters to something
// MongoDB accepts
function filter(languages, genres, rated) {
  let languageFilter = languages.map(language => {
    return({ Language: { $regex: language, $options: "i" } });
  });

  let genreFilter = genres.map(genre => {
    return({ Genre: { $regex: genre, $options: "i" } });
  });

  let ratedFilter = rated.map(rating => {
    return({ Rated: rating });
  });

  let filters = [];

  // Since MongoDB does not allow empty arrays in
  // $and or $or, an empty object has to be passed.
  if(languageFilter.length == 0)
    languageFilter = [{}];
  if(genreFilter.length == 0)
    genreFilter = [{}];
  if(ratedFilter.length == 0)
    ratedFilter = [{}];

  filters.push(languageFilter, genreFilter, ratedFilter);

  return (filters);
}

// Sorting logic
function sorting(sortOrder, sortType) {
  let sortBy = {};

  switch(sortOrder) {
    case 'ASC':
      sortOrder = 1;
      break;
    case 'DESC':
      sortOrder = -1;
      break;
    default:
      sortOrder = 1;
  }

  switch(sortType) {
    case 'Year':
      sortBy = { "Year": sortOrder };
      break;
    case 'Box office':
      sortBy = { "BoxOffice": sortOrder };
      break;
    case 'User rating':
      sortBy = { "UserRating": sortOrder };
      break;
    case 'IMDB rating':
      sortBy = { "imdbRating": sortOrder };
      break;
    default:
      sortBy = { "Title": sortOrder };
  }

  return sortBy;
}

/*
 * GET movie headers
 *
 * Params: {
 *   Title: title of movie
 * }
 * Returns: {
 *   success: Boolean,
 *   data: the title and user rating of the title
 * }
 *
 * This data is used int the result list
 */
router.get('/GetMovieHeaders', function(req, res, next) {
  var query = Movies.findOne({ "Title": req.query.Title }, { "Title": 1, "UserRating": 1 });

  query.exec(function(err, data) {
    if (err) return res.json({ success: false, error: err });

    return res.json({ success: true, data: data });
  })
})


/* POST user ratings
 *
 * Params: {
 *   Title: title of movie,
 *   UserRating: the given rating to add
 * }
 * Returns: { success: Boolean } - if the update was successful or not
 *
 * Updates or sets the user rating for the given movie.
 * First get the current user ratings from DB, add them
 * all together with the new rating and calculate the
 * average. If no ratings exists in DB, add only the new.
 *
 * Update the collection in DB with new ratings and average
 */
router.post('/UpdateUserRating', function(req, res, next) {
  Movies.find({ "Title" : req.query.Title }, function(err, data) {
    if (err) return res.json({ success: false, error: err });

    let totalRating = 0;
    let averageRating = 0;

    let tempUserRatings = data[0].UserRatings;

    // Calculates the average rating
    if (data[0].UserRatings.length !== 0) {
      data[0].UserRatings.map((rating) => {
        totalRating += Number(rating);
      });

      averageRating = (totalRating + Number(req.query.UserRating)) / (data[0].UserRatings.length + 1);
    } else {
      averageRating = req.query.UserRating;
    }

    tempUserRatings.push(req.query.UserRating);

    Movies.updateOne( { "Title": req.query.Title }, { $set: {
      "UserRating": averageRating,
      "UserRatings": tempUserRatings
    }}, (err, data) => {
      if (err) return res.json({ success: false, error: err });

      return res.json({ success: true });
    });
  });
});

/*
 * GET MovieTrends
 *
 * Params: {
 *   Title: the movie title to get trends on
 * }
 * Returns: { success: Boolean, data: the result from Google }
 *
 * Uses the Google Trends API to get the interest over time on the
 * selected movie title. Used to get the graph view on each movie.
 */
router.get('/MovieTrend', function(req, res, next) {
  // Get the date 7 days ago.
  var days = 7;
  var date = new Date();
  var lastWeek = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
  var lastDay = lastWeek.getDate();
  var lastMonth = lastWeek.getMonth() + 1;
  var lastYear = lastWeek.getFullYear();

  var startTime = new Date(lastYear + "-" + lastMonth + "-" + lastDay);

  googleTrends.interestOverTime({
    keyword: req.query.Title,
    startTime: startTime
  })
  .then(function(result) {
    result = JSON.parse(result)
    let tempData = [];
    let tempTitles = ['Date', 'Interest'];
    tempData.push(tempTitles);

    // Since the data recieved from Google is not on the form we want,
    // some magic has to happen. Created the data structure that
    // react-google-charts wants
    result.default.timelineData.map(time => {
      let tempValues = [];
      tempValues.push(time.formattedTime);
      tempValues.push(Number(time.formattedValue[0]));
      tempData.push(tempValues);
      return "";
    });
    if (tempData.length > 1) {
      return res.json({ success: true, data: tempData });
    } else {
      return res.json({ success: false, error: 'No data found' })
    }
  })
  .catch(function(error) {
    return res.json({ success: false, error: error });
  });
})

module.exports = router;
