const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/*
 * How the object stored in MongoDB collection is structured
 */
const MoviesSchema = new Schema({
  Title: String,
  Year: Number,
  Rated: String,
  Released: String,
  Runtime: String,
  Genre: String,
  Director: String,
  Writer: String,
  Actors: String,
  Plot: String,
  Language: String,
  Country: String,
  Awards: String,
  Poster: String,
  Ratings: Array,
  Metascore: Number,
  imdbRating: Number,
  imdbVotes: String,
  imdbID: String,
  Type: String,
  DVD: String,
  BoxOffice: String,
  Production: String,
  Website: String,
  Response: String,
  UserRatings: Array,
  UserRating: String
});

module.exports = mongoose.model("Movies", MoviesSchema);
