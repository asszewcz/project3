# Documentation
### REST API
The group decided early to develop a REST API using `express` and `mongoose` with a MongoDB database. This because a group member already had expreience using this, and an API with several, distinct, end points, is more perferrable than a `GraphQL` backend in our opinion. The code is naturally more readable and the functionality is easier to understand for someone with less experience in backend programming. 

The backend is made such that most of the data handling and computation is done on the server instead of on the client. This to make the site more snappy and faster. 
The content is dynamically loaded to the client as the user navigates on the site. The search result is updated live when the user types in the search box, or when a filter or sort is applied. Only 10 results are loaded for each change, and the 10 results are updated when navigating to a different page. The downside of this is that the number of calls to the database is huge. One optimization could have been to store the initial result set in the back end and return parts of it according to the inputs from frontend. This could easily have been utilized with sort and filter, and only have a database call when changing the search input.

The group tried to minimalize the amount of data sent from server to client by dividing the backend into several different end points that handles a sall query each.

### CSS and react-bootstrap
Since the project allowed us to use third party packages, we felt `react-bootstrap` was a must. This allowed us to focus on functionality instead of component creation. This package contains numerous components that are already designed and functionality is easy to implement. Among others we used
```javascript
Button
Dropdown
Accordion
Pagination
```
from `react-bootstrap`. The group felt these components was a natural choice based on our own experience to enhance UX.

As for our CSS we enjoyed making the site responisve even though this was not an requirement. It should work properly on both desktop, laptops and mobile devices. The site is not optimized for tablets, but should be usable nontheless. We used CSSs GridLayout since this was utilized in project 2, and the group already had experience with it.

For the user rating stars we used [Material Icons](https://material.io/resources/icons/?style=baseline). These icons are very beautiful and easy to use.

### General design and layout
We heavily relied on [Komplett.no](https://www.komplett.no)s filter and sort functionality as inspiration. We all felt this was an intuitive way of handling this. No filters selected behaves exactly like if all filters are selected. The filter functionality is inclusive or. When two filters are selected, the user will get the results of either one of them. Since the user can filter on official age ratings, it's intuitive to get movies that are either R rated or G rated when selecting these filters. No movie is rated both R and G. We decided to have this functionality on all three filter methods as consistency is important for good UX-design. The filter component also includes scrollable elements to minimize the size of it. 
Due to time constraints, the rendering of each checkbox is hard coded. If time had allowed it, we would have printed these out using some sort of iteration to make the code more clean and understandable.

We limited the number of results per page to 10 as this did not make the site too crowded. This way we could also show off our pagination more frequently. We chose to use `Accordion` from `react-bootstrap` to show the search results. This makes the result list clear and consistant, and easy to use. When the result is clicked the `Accordion` opens and details about the selected movie is shown. The poster is taken from Amazons webservice via. a URL we got from the source of the data ([discussed below](#external-sources)). The Google Trends Chart is discussed [here](#advanced-view).

Dropdown was chosen for the sort functionality as the user should only be able to select one. Radio-buttons would have made the site too crowded. The sort order is a single button which alternates between two states, ascending and descending.

We chose a pagination system instead of dynamic scrolling as this reduces the load on the client side. If the user scrolls far enough down, the data on the client will grow and the site will become slower and slower. A pagination system is preferable since the amount of data on the client is restricted by the server. It also looks better according to us. The system itself is purly `react-bootstrap` with added functionality. Same as with the filter component, the actual render if the pagination could have been handled better. This was not improved as time ran out.

### Advanced view
As we understood the requirment "Advanced view" both from the project description and the Piazza forum, we had to show some sort of data using some third part packages. We decided to use the `google-trends-api` to get the [Google Trends](https://trends.google.com) data for the selected movie title. This API call returns a `JSON`-object containing date and amount of searches that day. After some restructuring of the data we passed it to the `Chart` component from the `react-google-charts` package. As both of these packages are developed by Google, they are easy to understand and easy to use.

We also included the number of results of the given query above the result list, just to show the user exactly how many movies where mached. 

### Deployment
At first we had some problems with deployment as the client tried to send API request to the wrong port. This was solved by setting `axios.defaults.baseURL` in `App.js` to the REST API URL. We then allowed CORS headers to each call, and everything went well.
The API is run on a detached screen using `screen` on Ubuntu. This way you can logout and the server will keep running as long as the VM is.

### External sources
Since we decided to have a movie database, the natural choice of inspiration was [IMDb](https://www-imdb.com), however the API provided is pay to use. We scoured the web and stumbled upon [OMDb](https://omdbapi.com/) which is a free to use movie API with restructions on the number of calls per hour. The task required us to have our own database, so we wrote a webscraper that gathered the output from [OMDb](https://omdbapi.com/) on 250 movie titles, and stored it in a `.json` file. This we imported to our own MongoDB collection.

### Testing
In this project we have used `jest` for unit and snapshot-testing, and `cypress` for the end-to-end testing (E2E).

##### Unit testing
The unit testing in this project primarily focus on our implementation of `redux`. This is due to most of the functionality being handled in our `reducers` and `actions`. As an example we could look at the tests written in `actions.test.js`. An action in redux usually takes in a parameter, handles it in some way, and returns the desired json-object to be handled by a reducer. Testing an action is therefore fairly simple, as we only need to feed it a logical argument, and see check that the action returns the wanted json-object.

##### Snapshot testing
Snapshot testing is done on the main component `App`, and also on any sub-component. The tests can be found alongside each of their component files.

##### End-to-End testing
E2E is done using the `cypress` testing tools in order to achieve a systematic test of the functionality and flow of the webpage. The tests focus on the main things that the webpage offer; searching, filtering, ordering and sorting movies. Each result page only houses 10 results, so we could search something very general, like the letter 's', and sort by year to check if a very old movie pops up, if the order button is pressed, and the movie is still there, it would tell us that ordering might not be correct since it should be on the later pages. E2E testing code is located in `client/cypress/integration/home_page_spec.js`.

### Redux
We had no experience in using state management tools, so we did some reasearch on both `Redux` and `Mobx` before deciding to use the former as a data store in our project. Since both filter and sort functionality was required for the web site we implemented the state for both these components in our store, as well as the activation state of the filter and the string used to search. We found it to be sufficient to store these states in redux, as the remaining states are not needed outside of the App component, and we wanted to reduce the number of states stored in redux.

The redux implementation follows the standard setup, with action creators prompting the store for state changes in the store, and reducers for each state that handles the change. We did make good use of the "mapStateToProps" for example to feed the state directly into the RestAPI calls.

---
---
---

# Project3
## Install
```bash
git clone
cd Project3/client
npm install
cd ../server
npm install
```

## Run
Until we set up nodeamon we'll use the old way. In one terimal/GIT Bash:
```bash
cd Project3/client
npm start
```
In another:
```bash
cd Project3/server
PORT=3001 node server.js
```

## MongoDB shit
Username: gruppe29
Password: sniperxXx42069

| **Route** | **Method** |    **Path**           |                        **Description**                       |     **Parameters**     |           **Returns**          |
| --------- | ---------- | --------------------- | ------------------------------------------------------------ | ---------------------- | ------------------------------ |
| movies.js | GET        | /api/GetMovies        | Get movies from "from" to "to" that mathces the searchString | searchString, from, to | { success: Boolean, data: {} } |
| movies.js | GET        | /api/Search           | Get the 5 first result of searchString                       | searchString           | { data: {} }                   |
| movies.js | POST       | /api/UpdateUserRating | Updates or sets the user ratings                             | Title, UserRating      | { success: Boolean }           |

### How to use `axios`
In general
```javascript
axios.METHOD(PATH, params: { PARAMS })
.then((res) => {
  // DO SHIT WITH RESPONSE
});
```
Search
```javascript
axios.get('/api/Search', params: { searchString: 'somestring' })
.then((res) => {
  this.setState({ searchResult: res.data.data });
});
```
GetMovies
```javascript
axios.get('/api/GetMovies', params: { searchString: 'somestring', from: 0, to: 5})
.then((res) => {
  this.setState({ movies: res.data.data });
});
```

Since `axios.post` takes the parameters as the third argument, `null` has to be passed as the second.
UpdateUserRating
```javascript
axios.post('/api/UpdateUserRating', null, { params: { Title: 'something', UserRating: N }});
```

### How to use data
Use `data.Title` to get the title etc. To iterate through a result set use
```javascript
data.map((movie, index) => {
  <div>{movie.Title} - {movie.Genre}</div>
})
```

###  data structure
```javascript
Title: String,
Year: Number,
Rated: String,
Released: String,
Runtime: String,
Genre: String,
Director: String,
Writer: String,
Actors: String,
Plot: String,
Language: String,
Country: String,
Awards: String,
Poster: String,
Ratings: Array,
Metascore: Number,
imdbRating: Number,
imdbVotes: String,
imdbID: String,
Type: String,
DVD: String,
BoxOffice: String,
Production: String,
Website: String,
Response: String,
UserRatings: Array,
UserRating: Number
```

### Running tests
In order to run the `unit` and `snapshot` tests use
```javascript
npm test
```

For `end-to-end` testing use
```javascript
npx cypress open
```
